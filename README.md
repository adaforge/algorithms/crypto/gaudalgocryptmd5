# Purpose

md5 hashing of a file.

# Build Configuration

We use the `gnatkr` tool to get a (mostly) unique executable name on 16 (max) caracter length.
See the [gnatkr documentation](https://docs.adacore.com/gnat_ugn-docs/html/gnat_ugn/gnat_ugn/the_gnat_compilation_model.html#file-name-krunching-with-gnatkr) for the File Name Krunching algorithm.

```shell
$ cd gpr
$ gnatkr gaudry_algorithms_crypto_md5_tests.gpr 16
$ cd ..
```

```ada
   for Main use ("testsuite.adb");

   for Executable ("testsuite.adb") use "gaualgcrymd5test";  -- gnatkr *.adb 16
```

# Build

```shell
$ gprbuild
```

or

```shell
$ alr build
```

Results in:

```
build
└── development (or validation)(or release)
    └── x86_64-apple-darwin22.1.0
        ├── bin
        ├── lib
        └── obj
```

# Run

### On Unix-like

There could be a soft-link to the `build/*/*/bin` directory

```shell
$ ln -sv build/development/x86_64-apple-darwin22.1.0/bin bin
```

then,

```shell
$ bin/gaualgcrymd5test
```

### On Window$

?? up to you ...

```
build\development\x86_64\bin\gaualgcrymd5test.exe
```
