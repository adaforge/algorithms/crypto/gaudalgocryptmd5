--
--
--      M           M    DDDDDDDDDDDDDDDD        555555555555555
--      MM         MM       D           D        5
--      M M       M M       D           D        5
--      M  M     M  M       D           D        5
--      M   M   M   M       D           D        5
--      M    M M    M       D           D        5555555555555
--      M     M     M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                    5
--      M           M    DDDDDDDDDDDDDDDD       55555555555555
--
--
--
--     COPYRIGHT (C)  2005  DANIEL GAUDRY (DANIEL@DGAUDRY.COM)
--     9 AV CALMELS
--     92270 BOIS COLOMBES  FRANCE
--     HOME +33147862334
--     CELL +33664195009
 
--
--     THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
--     MODIFY IT UNDER THE TERMS OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
--     VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
--
--     THIS LIBRARY IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,
--     BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
--     MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  SEE THE GNU
--     LIBRARY GENERAL PUBLIC LICENSE FOR MORE DETAILS.
--
--     YOU SHOULD HAVE RECEIVED A COPY OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE ALONG WITH THIS LIBRARY; IF NOT, WRITE TO THE FREE
--     SOFTWARE FOUNDATION, INC., 59 TEMPLE PLACE - SUITE 330, BOSTON,
--     MA 02111-1307, USA
--
--|
--|
--|
--|--××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
--|--××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
--| VERSION 1.00 JAN         02 ND 2005 INITIAL CODING
--| VERSION 1.1 APRIL        12 TH 2021 ADDED BARS
--|--××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
--|--××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××××
 
with Gaudry.Algorithms.Crypto.Md5_1;
use Gaudry.Algorithms.Crypto;

with Ada.Text_Io;
--|
--|
--|                    _________________________________
--|                   ||×××××××××××××××××××××××××××××××||
--|                   ||×                             ×||
--|                   ||×          M D 5              ×||
--|                   ||×                             ×||
--|                   ||×_____________________________×||
--|                   ||×××××××××××××××××××××××××××××××||
--|
 
procedure TestSuite
   is
   File_Name : constant String := "src/test/data/TEST.PDF";
   begin
 
   Ada.Text_Io.Put_Line("MD5");
   Ada.Text_Io.New_Line;
   Md5_1.Process(The_File => File_Name);
   Ada.Text_Io.New_Line;
   Ada.Text_Io.Put_Line("EXPECTED RESULTS  FOR " & File_Name & ":");
   Ada.Text_Io.Put_Line("EF5A20EC 9BA41351 F8323BE2 A1DAF243 15C3069E");
   Ada.Text_Io.New_Line;
 
end TestSuite;
