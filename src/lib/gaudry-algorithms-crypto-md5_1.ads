--
--
--      M           M    DDDDDDDDDDDDDDDD        555555555555555
--      MM         MM       D           D        5
--      M M       M M       D           D        5
--      M  M     M  M       D           D        5
--      M   M   M   M       D           D        5
--      M    M M    M       D           D        5555555555555
--      M     M     M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                    5
--      M           M    DDDDDDDDDDDDDDDD       55555555555555
--
--
--
--     COPYRIGHT (C)  2005  DANIEL GAUDRY (DANIEL@DGAUDRY.COM)
--     9 AV CALMELS
--     92270 BOIS COLOMBES  FRANCE
--     HOME +33147862334
--     CELL +33664195009
 
--
--     THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
--     MODIFY IT UNDER THE TERMS OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
--     VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
--
--     THIS LIBRARY IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,
--     BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
--     MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  SEE THE GNU
--     LIBRARY GENERAL PUBLIC LICENSE FOR MORE DETAILS.
--
--     YOU SHOULD HAVE RECEIVED A COPY OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE ALONG WITH THIS LIBRARY; IF NOT, WRITE TO THE FREE
--     SOFTWARE FOUNDATION, INC., 59 TEMPLE PLACE - SUITE 330, BOSTON,
--     MA 02111-1307, USA
--
--| VERSION 1.00 JAN         02 ND 2005 INITIAL CODING
 
with Ada.Text_Io;
with Gaudry.Algorithms.Crypto.Md5_Data;
--|
--|
--|                    _________________________________
--|                   ||�������������������������������||
--|                   ||                               ||
--|                   ||          MD5_1                ||
--|                   ||                               ||
--|                   ||_______________________________||
--|                    ���������������������������������
--|
package Gaudry.Algorithms.Crypto.Md5_1
   is
 
   package Atio renames Ada.Text_Io;
   --|
   --|                         |==================================|
   --|                         |         PROCESS                  |
   --|                         |==================================|
   --|
   procedure Process(The_File : in String);
   --|
   --|
   --|
   private
   W    : Md5_Data.K_Array_Type := (others => 0);
   H    : Md5_Data.H_Array_Type := Md5_Data.H_Initial;
   A    : Md5_Data.Modulo;
   B    : Md5_Data.Modulo;
   C    : Md5_Data.Modulo;
   D    : Md5_Data.Modulo;
   E    : Md5_Data.Modulo;
   Temp : Md5_Data.Modulo;
 
 
end Gaudry.Algorithms.Crypto.Md5_1;
