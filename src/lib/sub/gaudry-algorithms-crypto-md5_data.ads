--
--
--      M           M    DDDDDDDDDDDDDDDD        555555555555555
--      MM         MM       D           D        5
--      M M       M M       D           D        5
--      M  M     M  M       D           D        5
--      M   M   M   M       D           D        5
--      M    M M    M       D           D        5555555555555
--      M     M     M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                    5
--      M           M    DDDDDDDDDDDDDDDD       55555555555555
--
--
--     COPYRIGHT (C)  2005  DANIEL GAUDRY (DANIEL@DGAUDRY.COM)
--     9 AV CALMELS
--     92270 BOIS COLOMBES  FRANCE
--     HOME +33147862334
--     CELL +33664195009
 
--
--     THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
--     MODIFY IT UNDER THE TERMS OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
--     VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
--
--     THIS LIBRARY IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,
--     BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
--     MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  SEE THE GNU
--     LIBRARY GENERAL PUBLIC LICENSE FOR MORE DETAILS.
--
--     YOU SHOULD HAVE RECEIVED A COPY OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE ALONG WITH THIS LIBRARY; IF NOT, WRITE TO THE FREE
--     SOFTWARE FOUNDATION, INC., 59 TEMPLE PLACE - SUITE 330, BOSTON,
--     MA 02111-1307, USA
--
--| VERSION 1.00 JAN         02 ND 2005 INITIAL CODING
 
with Ada.Text_Io;
with Unchecked_Conversion;
--|
--|
--|                    _________________________________
--|                   ||�������������������������������||
--|                   ||                               ||
--|                   ||              MD5              ||
--|                   ||                               ||
--|                   ||_______________________________||
--|                    ���������������������������������
--|
package Gaudry.Algorithms.Crypto.Md5_Data
   is
   --|
   --|$$$$$$$$$$$$$$$$$$$$$$$$$ DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
   --|
   Debug_On : constant Boolean := False;
   --|
   --|$$$$$$$$$$$$$$$$$$$$$$$$$ END DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
   --|
 
   --|
   --| DATA TYPES FROM STANDARD
   --|
   File_Size : Long_Long_Integer := 0;
   Mod_Size  : constant Integer  := 32;
   type Modulo is mod 2 ** Mod_Size;
 
   package Debug_Modulo is new Ada.Text_Io.Modular_Io(Modulo);
   --
   subtype Block_Type is String(1 .. 2 * Mod_Size);
   --
   type Word_Array_Type is array (Integer range 1 .. 16) of Modulo;
   --
   type K_Array_Type is array (Integer range 0 .. 79) of Modulo;
   --
   type H_Array_Type is array (Integer range 0 .. 4) of Modulo;
   --
   subtype Message_Digest_Type is String(1 .. 20);
   --|
   --| THIS FUNCTION ASSIGNS THE SAME MEMORY ADRESS TO THE BLOCK_TYPE TYPE SUBSTRING
   --| AND WORD_ARRAY_TYPE AND RETURNS THE CORRESPONDING WORD_ARRAY_TYPE VALUE
   --|
   function Block_Array_To_Word_Array is new Unchecked_Conversion(Source => Block_Type,
                                                                  Target => Word_Array_Type);
   --|
   --| THIS FUNCTION ASSIGNS THE SAME MEMORY ADRESS TO THE BLOCK_TYPE TYPE SUBSTRING
   --| AND WORD_ARRAY_TYPE AND RETURNS THE CORRESPONDING BLOCK_TYPE VALUE
   --|
   function Word_Array_To_Block_Array is new Unchecked_Conversion(Source => Word_Array_Type,
                                                                  Target => Block_Type);
   --|
   --| THIS FUNCTION ASSIGNS THE SAME MEMORY ADRESS TO H_ARRAY_TYPE
   --| AND MESSAGE_DIGEST_TYPE AND RETURNS THE CORRESPONDING MESSAGE_DIGEST_TYPE VALUE
   --|
   function H_To_Message_Digest is new Unchecked_Conversion(Source => H_Array_Type,
                                                            Target => Message_Digest_Type);
   --|
   --|CONSTANTS FROM STANDARD
   --|
 
   K : constant K_Array_Type := (00 .. 19 => (16#5A827999#),
                                 20 .. 39 => (16#6ED9EBA1#),
                                 40 .. 59 => (16#8F1BBCDC#),
                                 60 .. 79 => (16#CA62C1D6#));
   --|
   --| INITIAL VALUES FROM STANDARD
   --|
   H_Initial : constant H_Array_Type := (0 => 16#67452301#,
                                         1 => 16#EFCDAB89#,
                                         2 => 16#98BADCFE#,
                                         3 => 16#10325476#,
                                         4 => 16#C3D2E1F0#);
   --|
   --| MOD 64 IS BIG ENDIAN AND CHAR STORAGE IS LITTLE ENDIAN SO WE HAVE TO TRANSLATE
   --|
   subtype Index_Type is Integer range Block_Type'Range;
   type Endian_Change_Table_Type is array (Index_Type'Range) of Index_Type;
   Endian_Change_Table : constant Endian_Change_Table_Type :=
   (004, 003, 002, 001,
    008, 007, 006, 005,
    012, 011, 010, 009,
    016, 015, 014, 013,
    020, 019, 018, 017,
    024, 023, 022, 021,
    028, 027, 026, 025,
    032, 031, 030, 029,
    036, 035, 034, 033,
    040, 039, 038, 037,
    044, 043, 042, 041,
    048, 047, 046, 045,
    052, 051, 050, 049,
    056, 055, 054, 053,
    060, 059, 058, 057,
    064, 063, 062, 061);
 
end Gaudry.Algorithms.Crypto.Md5_Data;
