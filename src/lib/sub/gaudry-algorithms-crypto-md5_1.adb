--
--
--      M           M    DDDDDDDDDDDDDDDD        555555555555555
--      MM         MM       D           D        5
--      M M       M M       D           D        5
--      M  M     M  M       D           D        5
--      M   M   M   M       D           D        5
--      M    M M    M       D           D        5555555555555
--      M     M     M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                    5
--      M           M    DDDDDDDDDDDDDDDD       55555555555555
--
--
--
--
--
--
--
--     COPYRIGHT (C)  2005  DANIEL GAUDRY (DANIEL@DGAUDRY.COM)
--     9 AV CALMELS
--     92270 BOIS COLOMBES  FRANCE
--     HOME +33147862334
--     CELL +33664195009


with Gaudry.Algorithms.Crypto.Md5_2;
with Gaudry.Algorithms.Crypto.Md5_Debug_1;
with Unchecked_Conversion;
with Ada.Directories;

--|
--|
--|                    _________________________________
--|                   ||_______________________________||
--|                   ||                               ||
--|                   ||          MD5_1                ||
--|                   ||                               ||
--|                   ||_______________________________||
--|                    _________________________________
--|
package body Gaudry.Algorithms.Crypto.Md5_1 
   is 
   Count : Long_Long_Integer := 0;
   --|
   --|                         |==================================|
   --|                         |                FT                |
   --|                         |==================================|
   --|
   function Ft(T : in Integer;
               B : in Md5_Data.Modulo;
               C : in Md5_Data.Modulo;
               D : in Md5_Data.Modulo) 
      return Md5_Data.Modulo 
      is 
      use type Md5_Data.Modulo;
      begin 
      
      case T is 
         
         -- F       FT(B,C,D) = (B AND C) OR ((NOT B) AND D) ( 0 <= T <= 19)
         when 0 .. 19 => 
         return (B and C) or ((not B) and D);
         
         --G        FT(B,C,D) = B XOR C XOR D (20 <= T <= 39)
         when 20 .. 39 => 
         return B xor C xor D;
         
         --H        FT(B,C,D) = (B AND C) OR (B AND D) OR (C AND D) (40 <= T <= 59)
         when 40 .. 59 => 
         return (B and C) or (B and D) or (C and D);
         
         --I        FT(B,C,D) = B XOR C XOR D (60 <= T <= 79).
         when 60 .. 79 => 
         return B xor C xor D;
         
         when others => 
         return 0;
      end case;
      
   end Ft;
   
   pragma Inline(Ft);
   --|
   --|                         |==================================|
   --|                         |                SN                |
   --|                         |==================================|
   --|
   function Sn(N : in Positive;
               X : in Md5_Data.Modulo) 
      return Md5_Data.Modulo 
      is 
      use type Md5_Data.Modulo;
      begin 
      return (X * 2 ** N) or (X / 2 ** (Md5_Data.Mod_Size - N));
   end Sn;
   
   pragma Inline(Sn);
   --|
   --|                         |==================================|
   --|                         |  ADD_LENGTH_TO_LAST_BLOCK        |
   --|                         |==================================|
   --|
   procedure Add_Length_To_Last_Block(Message_Block : out Md5_Data.Block_Type;
                                      L             : in Md5_Data.Modulo) 
      is 
      --|
      --| THIS FUNCTION ASSIGN THE SAME MEMORY ADRESS TO THE BLOCK_TYPE TYPE SUBSTRING
      --| AND WORD_ARRAY_TYPE AND RETURN THE CORRESPONDING WORD_ARRAY_TYPE VALUE
      --|
      subtype Two_String_Type is String(1 .. 8);
      type Mod_64 is mod 2 ** 64;
      function Long_Long_To_Two_String_Type is new Unchecked_Conversion(Source => Mod_64, 
                                                                        Target => Two_String_Type);
      
      Two_String : Two_String_Type := (others => ' ');
      Dummy      : constant Mod_64 := Mod_64(L);
      begin 
      --|
      --| GET INTO A STRING
      --|
      Two_String := Long_Long_To_Two_String_Type(S => Dummy);
      --|
      --| PUT AT THE END OF THE BLOCK
      --|
      Message_Block(Message_Block'Last - 7 .. Message_Block'Last) := Two_String(5 .. 8) & Two_String(1 .. 4);
   end Add_Length_To_Last_Block;
   
   pragma Inline(Add_Length_To_Last_Block);
   --|
   --|                         |==================================|
   --|                         |   ADD_CURRENT_BLOCK_TO_HASH      |
   --|                         |==================================|
   --|
   procedure Add_Current_Block_To_Hash(Message_Block : in Md5_Data.Block_Type) 
      is 
      use type Md5_Data.Modulo;
      Word_Array : Md5_Data.Word_Array_Type;
      begin 
      --|
      --|  TRANSLATE FROM CHAR TO 64 BITS
      --|
      Word_Array := Md5_Data.Block_Array_To_Word_Array(Message_Block);
      --|
      --|  LOAD FIRST 16 VALUES
      --|
      for I in 1 .. 16 loop 
         W(I - 1) := Word_Array(I);
      end loop;
      --|
      --|  COMPUTE THE REST FROM STANDARD
      --|
      for T in 16 .. 79 loop 
         W(T) := Sn(N => 1, X => W(T - 3) xor W(T - 8) xor W(T - 14) xor W(T - 16));
      end loop;
      --|
      --| DONE LOAD FROM H
      --|
      A := H(0);
      B := H(1);
      C := H(2);
      D := H(3);
      E := H(4);
      --|
      --| GET VARIABLE FOR STANDARD CALCULATIONS
      for T in 0 .. 79 loop 
         
         Temp := Sn(N => 5, 
                    X => A) 
         + Ft(T       => T, 
              B       => B, 
              C       => C, 
              D       => D) 
         + E 
         + W(T) 
         + Md5_Data.K(T);
         --
         --
         -- E = D; D = C; C = S30(B); B = A; A = TEMP;
         E := D;
         D := C;
         C := Sn(N => 30, X => B);
         B := A;
         A := Temp;
         --|
         --|$$$$$$$$$$$$$$$$$$$$$$$$$ DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
         --|
         if Md5_Data.Debug_On then 
            Ada.Text_Io.Put(" T " & Integer'Image(T) & " ");
            Md5_Debug_1.Print_Hex(X => A);
            Md5_Debug_1.Print_Hex(X => B);
            Md5_Debug_1.Print_Hex(X => C);
            Md5_Debug_1.Print_Hex(X => D);
            Md5_Debug_1.Print_Hex(X => E);
            Md5_Debug_1.Print_Hex(X => Temp);
            Ada.Text_Io.New_Line;
         end if;
         --|
         --|$$$$$$$$$$$$$$$$$$$$$$$$$ END DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
         --|
      end loop;
      --|
      --|  LOAD FOR NEXT BLOCK IF ANY
      --|
      H(0) := H(0) + A;
      H(1) := H(1) + B;
      H(2) := H(2) + C;
      H(3) := H(3) + D;
      H(4) := H(4) + E;
      --|
      --|$$$$$$$$$$$$$$$$$$$$$$$$$ DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
      --|
      if Md5_Data.Debug_On then 
         Ada.Text_Io.New_Line;
         Ada.Text_Io.Put_Line("H VALUES");
         Md5_Debug_1.Print_Hex(X => H(0));
         Md5_Debug_1.Print_Hex(X => H(1));
         Md5_Debug_1.Print_Hex(X => H(2));
         Md5_Debug_1.Print_Hex(X => H(3));
         Md5_Debug_1.Print_Hex(X => H(4));
         Ada.Text_Io.New_Line;
      end if;
      --|
      --|$$$$$$$$$$$$$$$$$$$$$$$$$ END DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
      --|
      
      -- ATIO.PUT_LINE(ADA.DIRECTORIES.FILE_SIZE'IMAGE(ADA.DIRECTORIES.SIZE(NAME => FILE_NAME)));
      -- MD5_DATA.FILE_SIZE :=NATURAL(ADA.DIRECTORIES.SIZE(NAME => FILE_NAME));
      Count := 1 + Count;
      if Count mod (Md5_Data.File_Size / 2000) = 0 
         then 
         Atio.Put('|');
      end if;
   end Add_Current_Block_To_Hash;
   pragma Inline(Add_Current_Block_To_Hash);
   --|
   --|                         |==================================|
   --|                         |         INIT_HASH                |
   --|                         |==================================|
   --|
   procedure Init_Hash 
      is 
      begin 
      --|
      --| SET UP DATA
      --|
      H := Md5_Data.H_Initial;
      A := H(0);
      B := H(1);
      C := H(2);
      D := H(3);
      E := H(4);
      W := (others => 0);
      --Atio.Put_Line("MD5_1 INIT_HASH FILE_SIZE " & Long_Long_Integer'Image(Md5_Data.File_Size));
   end Init_Hash;
   pragma Inline(Init_Hash);
   --|
   --|                         |==================================|
   --|                         |         PROCESS                  |
   --|                         |==================================|
   --|
   procedure Process(The_File : in String) 
      is 
      use type Md5_Data.Modulo;
      Block                              : Md5_Data.Block_Type := (others => Ascii.Nul);
      Number_Of_Zeros_To_Padd_Last_Block : Md5_Data.Modulo     := 0;
      Number_Of_Bits_Left_In_Last_Block  : Md5_Data.Modulo     := 0;
      First_Available_Character_In_Block : Integer             := 0;
      begin 
      --|
      --| INIT READING
      --|
      if not Md5_2.Init(File_Name => The_File) 
         then 
         Ada.Text_Io.Put("PROBLEM WITH FILE " & The_File);
         return;
      end if;
      --|
      --| INIT HASH
      --|
      Init_Hash;
      --|
      --| LOOP USING READING ITERATOR
      --|
      while not Md5_2.Over loop 
         --|
         --| GET BLOCK
         --|
         Block := Md5_2.Value;
         --|
         --|$$$$$$$$$$$$$$$$$$$$$$$$$ DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
         --|
         if Md5_Data.Debug_On then 
            Ada.Text_Io.Put_Line("BLOCK # " & Md5_Data.Modulo'Image(Md5_2.How_Many_Characters / 64));
            Md5_Debug_1.Print_Word_Array_Type_As_Hex(Md5_Data.Block_Array_To_Word_Array(Block));
         end if;
         --|
         --|$$$$$$$$$$$$$$$$$$$$$$$$$ END DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
         --|
         
         --|
         --|ADD CURRENT BLOCK TO HASH
         --|
         Add_Current_Block_To_Hash(Message_Block => Block);
         --|
         --| NEXT IF ANY
         --|
         Md5_2.Next;
      end loop;
      --|
      --| READ LAST ONE
      --|
      Block := Md5_2.Value;
      --|
      --|$$$$$$$$$$$$$$$$$$$$$$$$$ DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
      --|
      if Md5_Data.Debug_On then 
         Ada.Text_Io.Put_Line("READING LAST BLOCK FROM FILE "
                              & The_File 
                              & " AS ");
         Md5_Debug_1.Print_Word_Array_Type_As_Hex(Md5_Data.Block_Array_To_Word_Array(Block));
      end if;
      --|
      --|$$$$$$$$$$$$$$$$$$$$$$$$$ END DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
      --|
      
      --|
      --| THERE ARE 3 POSSIBILITIES:
      --| 1) THE LAST BLOCK IS NOT FILLED AND THERE IS SPACE TO PASTE THE 1 BIT    A N D   THE
      --|TOTAL NUMBER OF BITS IN THE CURRENT BLOCK
      --|
      --|2) THE SPACE LEFT IN THE LAST BLOCK IS NOT BIG ENOUGH AND
      --| 2A) THE BLOCK IS COMPLETELY FILLED AND THE IS NO SPACE LEFT TO PASTE THE 1 BIT:
      --| CREATE A NEW  BLOCK
      --|SET THE FIRST BIT TO ONE AND PASTE THE LENGTH AT THE END
      --|
      --|2B) THE 1 BIT IS ADDED TO THE CURRENT BLOCK AND A NEW BLOCK IS CREATED TO HOLD THE
      --|THE LENGTH AT THE END
      --|
      
      
      
      --|
      --| GET DATA ON MESSAGE LAST BLOCK TO CHECK FOR THE RIGHT CHOICE (SEE ABOVE)
      --|
      Number_Of_Zeros_To_Padd_Last_Block := (512 + (448 - (8 * Md5_2.How_Many_Characters + 1))) mod 512;
      Number_Of_Bits_Left_In_Last_Block  := 512 - (8 * Md5_2.How_Many_Characters mod 512);
      First_Available_Character_In_Block := 1 + Integer(Md5_2.How_Many_Characters mod 64);
      --|
      --|$$$$$$$$$$$$$$$$$$$$$$$$$ DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
      --|
      if Md5_Data.Debug_On then 
         Ada.Text_Io.Put_Line("HOW_MANY_CHARACTERS ="
                              & Md5_Data.Modulo'Image(Md5_2.How_Many_Characters) 
                              & " NUMBER_OF_ZEROS_TO_PADD_LAST_BLOCK ="
                              & Md5_Data.Modulo'Image(Number_Of_Zeros_To_Padd_Last_Block) 
                              & Ascii.Cr & Ascii.Lf 
                              & " NUMBER_OF_BITS_LEFT_IN_LAST_BLOCK "
                              & Md5_Data.Modulo'Image(Number_Of_Bits_Left_In_Last_Block) 
                              & " FIRST_AVAILABLE_CHARACTER_IN_BLOCK "
                              & Integer'Image(First_Available_Character_In_Block));
         Ada.Text_Io.Put_Line("LAST BLOCK BEFORE PADDING =");
         
         Md5_Debug_1.Print_Word_Array_Type_As_Hex(Md5_Data.Block_Array_To_Word_Array(Block));
      end if;
      --|
      --|$$$$$$$$$$$$$$$$$$$$$$$$$ END DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
      --|
      
      --|
      --|NEED AN EXTRA BLOCK????
      --|
      if Number_Of_Zeros_To_Padd_Last_Block > Number_Of_Bits_Left_In_Last_Block 
         --|
         --|  YES WE DO NEED AN EXTRA BLOCK
         --|
         then 
         --|
         --|SPACE LEFT FOR THE ADDED 1 IN CURRENT BLOCK?
         --|
         if First_Available_Character_In_Block > 1 
            then 
            --|
            --|PUT THE FIRST AVAILABLE BIT TO 1
            --|
            Block(Md5_Data.Endian_Change_Table(First_Available_Character_In_Block)) := Character'Val(128);
         end if;
         --|
         --|$$$$$$$$$$$$$$$$$$$$$$$$$ DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
         --|
         if Md5_Data.Debug_On then 
            Ada.Text_Io.Put_Line("ONE BEFORE LAST BLOCK AFTER PADDING =");
            Md5_Debug_1.Print_Word_Array_Type_As_Hex(Md5_Data.Block_Array_To_Word_Array(Block));
         end if;
         --|
         --|$$$$$$$$$$$$$$$$$$$$$$$$$ DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
         --|
         
         --|
         --|ADD CURRENT BLOCK TO HASH
         --|
         Add_Current_Block_To_Hash(Message_Block => Block);
         --|
         --|CREATE A NEW EMPTY BLOCK
         --|
         Block := (others => Ascii.Nul);
         --|
         --|  THE ADDED 1 ALREAD IN PREVIOUS BLOCK
         --|
         if not (First_Available_Character_In_Block > 1) 
            then 
            --|
            --| NO PUT THE FIRST AVAILABLE BIT TO 1
            --|
            Block(1) := Character'Val(128);
         end if;
         --|
         --|NO NEED FOR AN EXTRA BLOCK
         --|
      else 
         --|
         --| PUT THE FIRST AVAILABLE BIT TO 1  (BIG ENDIAN TRANSLATION NEEDED !!!!!!!!!)
         --|
         Block(Md5_Data.Endian_Change_Table(First_Available_Character_In_Block)) := Character'Val(128);
         
      end if;
      --|
      --|ADD LENGTH TO LAST BLOCK
      --|
      Add_Length_To_Last_Block(Message_Block => Block, 
                               L             => 8 * Md5_2.How_Many_Characters);
      --|
      --|$$$$$$$$$$$$$$$$$$$$$$$$$   DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
      --|
      if Md5_Data.Debug_On then 
         Ada.Text_Io.Put_Line("LAST BLOCK AFTER PADDING =");
         Md5_Debug_1.Print_Word_Array_Type_As_Hex(Md5_Data.Block_Array_To_Word_Array(Block));
      end if;
      --|
      --|$$$$$$$$$$$$$$$$$$$$$$$$$ END DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
      --|
      
      --|
      --|ADD LAST BLOCK TO HASH
      --|
      Add_Current_Block_To_Hash(Message_Block => Block);
      --|
      --|$$$$$$$$$$$$$$$$$$$$$$$$$   DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
      --|
      if Md5_Data.Debug_On then 
         Md5_Debug_1.Print_Word_Array_Type_As_Hex(Md5_Data.Block_Array_To_Word_Array(Block));
      end if;
      --|
      --|$$$$$$$$$$$$$$$$$$$$$$$$$ END DEBUG $$$$$$$$$$$$$$$$$$$$$$$$$
      --|
      
      --|
      --|          R E S U L T S
      --|
      Ada.Text_Io.New_Line(Spacing => 2);
      Ada.Text_Io.Put_Line("FILE : " & The_File & " DONE, H VALUES : ");
      Md5_Debug_1.Print_Hex(X => H(0));
      Md5_Debug_1.Print_Hex(X => H(1));
      Md5_Debug_1.Print_Hex(X => H(2));
      Md5_Debug_1.Print_Hex(X => H(3));
      Md5_Debug_1.Print_Hex(X => H(4));
      Ada.Text_Io.New_Line;
      
   end Process;
   
   pragma Inline(Process);
   
end Gaudry.Algorithms.Crypto.Md5_1;
