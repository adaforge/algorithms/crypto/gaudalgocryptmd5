--
--
--      M           M    DDDDDDDDDDDDDDDD        555555555555555
--      MM         MM       D           D        5
--      M M       M M       D           D        5
--      M  M     M  M       D           D        5
--      M   M   M   M       D           D        5
--      M    M M    M       D           D        5555555555555
--      M     M     M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                    5
--      M           M    DDDDDDDDDDDDDDDD       55555555555555
--
--
--
--     COPYRIGHT (C)  2005  DANIEL GAUDRY (DANIEL@DGAUDRY.COM)
--     9 AV CALMELS
--     92270 BOIS COLOMBES  FRANCE
--     HOME +33147862334
--     CELL +33664195009
 
--
--     THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
--     MODIFY IT UNDER THE TERMS OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
--     VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
--
--     THIS LIBRARY IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,
--     BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
--     MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  SEE THE GNU
--     LIBRARY GENERAL PUBLIC LICENSE FOR MORE DETAILS.
--
--     YOU SHOULD HAVE RECEIVED A COPY OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE ALONG WITH THIS LIBRARY; IF NOT, WRITE TO THE FREE
--     SOFTWARE FOUNDATION, INC., 59 TEMPLE PLACE - SUITE 330, BOSTON,
--     MA 02111-1307, USA
--
--| VERSION 1.00 JAN         02 ND 2005 INITIAL CODING
with Unchecked_Conversion;
with Ada.Directories;
--|
--|                    _________________________________
--|                   ||�������������������������������||
--|                   ||                               ||
--|                   ||          MD5_2                ||
--|                   ||                               ||
--|                   ||_______________________________||
--|                    ���������������������������������
--|
package body Gaudry.Algorithms.Crypto.Md5_2
   is
   --|
   --|                         |==================================|
   --|                         |         NEXT                     |
   --|                         |==================================|
   --|
   procedure Next
      is
      --|
      --| THIS FUNCTION ASSIGNS THE SAME MEMORY ADRESS TO THE BLOCK_TYPE TYPE SUBSTRING
      --| AND WORD_ARRAY_TYPE AND RETURN THE CORRESPONDING WORD_ARRAY_TYPE VALUE
      --|
      function Data_To_Block is new Unchecked_Conversion(Source => Stream_Data_Type,
                                                         Target => Md5_Data.Block_Type);
      --|
      --| SEEKS AND GETS DATA OT FILL A FULL BLOCK IF POSSIBLE, IF NOT SET DONE TO TRUE
      --|
      use type Md5_Data.Modulo;
      begin
      --|
      --| READ NEXT IF ANY ( IF NONE LENGTH =0 IF PARTLY EMPTY LENGTH <64 IF FILLED LENGTH=64
      --|
      Ada.Streams.Stream_Io.Read(File => The_File,
                                 Item => Data,
                                 Last => Length,
                                 From => Count);
      --|
      --| CLEAR
      --|
      B     := (others => Ascii.Nul);
      Dummy := (others => Ascii.Nul);
      --|
      --| ASSIGN SAME MEMORY STORAGE
      --|
      Dummy := Data_To_Block(Data);
      --|
      --| ENDIAN CHANGE
      --|
      for K in 1 .. Integer(Length) loop
         B(Md5_Data.Endian_Change_Table(K)) := Dummy(K);
      end loop;
 
      --MD5_DEBUG_1.PRINT_WORD_ARRAY_TYPE_AS_HEX
      --(MD5_DATA.BLOCK_ARRAY_TO_WORD_ARRAY(B));
      --|
      --| UPDATE
      --|
      Count                := Ada.Streams.Stream_Io.Count(2 * Md5_Data.Mod_Size) + Count;
      Number_Of_Characters := Md5_Data.Modulo(Length) + Number_Of_Characters;
      --|
      --| IF OVER UPDATE AND SET DONE TO TRUE
      --|
      if 2 * Md5_Data.Mod_Size > Integer(Length) -- DONE
         then
         Count := Count - 1;-- STARTS AT ONE
         if 0 = Length then
            -- EMPTY LAST READING REMOVE THE WHOLE BLOCK SIZE
            Count := Count - Ada.Streams.Stream_Io.Count(2 * Md5_Data.Mod_Size);
         end if;
         Done := True;
      end if;
 
   end Next;
 
   pragma Inline(Next);
   --|
   --|                         |==================================|
   --|                         |           INIT                   |
   --|                         |==================================|
   --|
   function Init(File_Name : in String)
      return Boolean
      is
      begin
      --|
      --| CLOSE IF PREVIOUS FILE ALREADY USED
      --|
      if Ada.Streams.Stream_Io.Is_Open(File => The_File)
         then
         Ada.Streams.Stream_Io.Close(File => The_File);
      end if;
      --ATIO.PUT_LINE("MD5_2 INIT 01" & FILE_NAME);
 
      --|
      --| SET TO ASCII.NUL, ZERO AND FALSE
      --|
      Done                 := False;
      Number_Of_Characters := 0;
      Count                := 1;
      --|
      --| TRY OPENING THE FILE
      --|
      Ada.Streams.Stream_Io.Open(File => The_File,
                                 Name => File_Name,
                                 Mode => Ada.Streams.Stream_Io.In_File);
 
 
      --ATIO.PUT_LINE("MD5_2 INIT 02" & FILE_NAME);
      --|
      --| SUCCESS GET FIRST BLOCK IF ANY :
      --|DEPARTURE FROM NORMAL ITERATORS: THE LOOP IS NOT DONE IF ONLY ONE BLOCK
      --|
      Next;
 
      --ATIO.PUT_LINE("MD5_2 INIT 03" & FILE_NAME);
 
      Md5_Data.File_Size := Long_Long_Integer(Ada.Directories.Size(Name => File_Name));
 
      --ATIO.PUT_LINE("INIT" & ADA.DIRECTORIES.FILE_SIZE'IMAGE(ADA.DIRECTORIES.SIZE(NAME => FILE_NAME)) & FILE_NAME);
      --|
      --| FILE OPENED
      --|
 
      --ATIO.PUT_LINE("MD5_2 INIT 04" & FILE_NAME);
      return True;
      --|
      --|   NO SUCH FILE ??
      --|
      exception
      when others =>
      Atio.Put_Line("THE FILE " & File_Name & " IS NOT FOUND...");
      Done := True;
      return False;
   end Init;
 
   pragma Inline(Init);
   --|
   --|                         |==================================|
   --|                         |           VALUE                  |
   --|                         |==================================|
   --|
   function Value
      return Md5_Data.Block_Type
      is
      begin
      return B;
   end Value;
 
   pragma Inline(Value);
   --|
   --|                         |==================================|
   --|                         |         OVER                     |
   --|                         |==================================|
   --|
   function Over
      return Boolean
      is
      begin
      return Done;
   end Over;
 
   pragma Inline(Over);
   --|
   --|                         |==================================|
   --|                         |         HOW_MANY_CHARACTERS      |
   --|                         |==================================|
   --|
   function How_Many_Characters
      return Md5_Data.Modulo
      is
      begin
      return Number_Of_Characters;
   end How_Many_Characters;
 
   pragma Inline(How_Many_Characters);
end Gaudry.Algorithms.Crypto.Md5_2;
