--
--
--      M           M    DDDDDDDDDDDDDDDD        555555555555555
--      MM         MM       D           D        5
--      M M       M M       D           D        5
--      M  M     M  M       D           D        5
--      M   M   M   M       D           D        5
--      M    M M    M       D           D        5555555555555
--      M     M     M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                    5
--      M           M    DDDDDDDDDDDDDDDD       55555555555555
--
--
--
--     COPYRIGHT (C)  2005  DANIEL GAUDRY (DANIEL@DGAUDRY.COM)
--     9 AV CALMELS
--     92270 BOIS COLOMBES  FRANCE
--     HOME +33147862334
--     CELL +33664195009
 
--
--     THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
--     MODIFY IT UNDER THE TERMS OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
--     VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
--
--     THIS LIBRARY IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,
--     BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
--     MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  SEE THE GNU
--     LIBRARY GENERAL PUBLIC LICENSE FOR MORE DETAILS.
--
--     YOU SHOULD HAVE RECEIVED A COPY OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE ALONG WITH THIS LIBRARY; IF NOT, WRITE TO THE FREE
--     SOFTWARE FOUNDATION, INC., 59 TEMPLE PLACE - SUITE 330, BOSTON,
--     MA 02111-1307, USA
--
--| VERSION 1.00 JAN         02 ND 2005 INITIAL CODING
 
 
 
with Ada.Text_Io;
--|
--|
--|                    _________________________________
--|                   ||�������������������������������||
--|                   ||                               ||
--|                   ||    MD5_DEBUG_1                ||
--|                   ||                               ||
--|                   ||_______________________________||
--|                    ���������������������������������
--|
package body Gaudry.Algorithms.Crypto.Md5_Debug_1
   is
 
   --|
   --|                         |==================================|
   --|                         |         PRINT_HEX                |
   --|                         |==================================|
   --|
   procedure Print_Hex(X : in Md5_Data.Modulo)
      is
      Mod_Length : constant Integer := X'Size / 4;-- NUMBER OF HEX DIGITS IN X
      S          : String(1 .. Mod_Length + 4);
      First      : Integer;
 
      begin
 
      Md5_Data.Debug_Modulo.Put(Item => X,
                                Base => 16,
                                To   => S);
 
      --|
      --|
      --|
      for I in reverse 1 .. Mod_Length + 3 loop
         if S(I) = '#'
            then
            First := I;
            exit;
         end if;
      end loop;
      --|
      --|
      --|
      for I in reverse 4 .. First loop
         S(I) := '0';
      end loop;
      --|
      --|
      --|
      Ada.Text_Io.Put(S(4 .. Mod_Length + 3)
                      & ' ');
 
   end Print_Hex;
 
   pragma Inline(Print_Hex);
   --|
   --|                         |==================================|
   --|                         | PRINT_WORD_ARRAY_TYPE_AS_HEX     |
   --|                         |==================================|
   --|
   procedure Print_Word_Array_Type_As_Hex(Word_Array : in Md5_Data.Word_Array_Type)
      is
      begin
      --|
      --|
      --|
      for M in 1 .. 4 loop
         --|
         --|
         --|
         for K in 4 * M - 3 .. 4 * M loop
            Print_Hex(Word_Array(K));
         end loop;
         Ada.Text_Io.New_Line;
 
      end loop;
      --|
      --|
      --|
      Ada.Text_Io.New_Line;
 
   end Print_Word_Array_Type_As_Hex;
 
   pragma Inline(Print_Word_Array_Type_As_Hex);
 
 
end Gaudry.Algorithms.Crypto.Md5_Debug_1;
