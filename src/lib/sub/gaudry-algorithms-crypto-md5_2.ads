--
--
--      M           M    DDDDDDDDDDDDDDDD        555555555555555
--      MM         MM       D           D        5
--      M M       M M       D           D        5
--      M  M     M  M       D           D        5
--      M   M   M   M       D           D        5
--      M    M M    M       D           D        5555555555555
--      M     M     M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                     5
--      M           M       D           D                    5
--      M           M    DDDDDDDDDDDDDDDD       55555555555555
--
--
--
--     COPYRIGHT (C)  2005  DANIEL GAUDRY (DANIEL@DGAUDRY.COM)
--     9 AV CALMELS
--     92270 BOIS COLOMBES  FRANCE
--     HOME +33147862334
--     CELL +33664195009
 
--
--     THIS LIBRARY IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR
--     MODIFY IT UNDER THE TERMS OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE AS PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER
--     VERSION 2 OF THE LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.
--
--     THIS LIBRARY IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,
--     BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
--     MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  SEE THE GNU
--     LIBRARY GENERAL PUBLIC LICENSE FOR MORE DETAILS.
--
--     YOU SHOULD HAVE RECEIVED A COPY OF THE GNU LIBRARY GENERAL PUBLIC
--     LICENSE ALONG WITH THIS LIBRARY; IF NOT, WRITE TO THE FREE
--     SOFTWARE FOUNDATION, INC., 59 TEMPLE PLACE - SUITE 330, BOSTON,
--     MA 02111-1307, USA
--
--| VERSION 1.00 JAN         02 ND 2005 INITIAL CODING
 
with Ada.Text_Io;
with Ada.Streams.Stream_Io;
with Gaudry.Algorithms.Crypto.Md5_Data;
--|
--|
--|                    _________________________________
--|                   ||�������������������������������||
--|                   ||                               ||
--|                   ||         MD5_2               ||
--|                   ||                               ||
--|                   ||_______________________________||
--|                    ���������������������������������
--|
package Gaudry.Algorithms.Crypto.Md5_2
   is
   package Atio renames Ada.Text_Io;
   --|
   --|                         |==================================|
   --|                         |           INIT                   |
   --|                         |==================================|
   --|
   function Init(File_Name : in     String) return Boolean;
   --|
   --|                         |==================================|
   --|                         |           VALUE                  |
   --|                         |==================================|
   --|
   function Value return Md5_Data.Block_Type;
   --|
   --|                         |==================================|
   --|                         |         OVER                     |
   --|                         |==================================|
   --|
   function Over return Boolean;
   --|
   --|                         |==================================|
   --|                         |         NEXT                     |
   --|                         |==================================|
   --|
   procedure Next;
   --|
   --|                         |==================================|
   --|                         |         HOW_MANY_CHARACTERS      |
   --|                         |==================================|
   --|
   function How_Many_Characters return Md5_Data.Modulo;
   --|
   --|
   --|
   File_Not_Found : exception;
   --|
   --|
   --|
   private
 
   use type Ada.Streams.Stream_Element_Offset;
   use type Ada.Streams.Stream_Io.Count;
   subtype Stream_Data_Type is Ada.Streams.Stream_Element_Array(1 .. Ada.Streams.Stream_Element_Offset(2 * Md5_Data.Mod_Size));
 
   The_File             : Ada.Streams.Stream_Io.File_Type;
   Number_Of_Characters : Md5_Data.Modulo := 0;
   B                    : Md5_Data.Block_Type;
   Dummy                : Md5_Data.Block_Type;
   Data                 : Stream_Data_Type;
   Length               : Ada.Streams.Stream_Element_Offset := Ada.Streams.Stream_Element_Offset(2 * Md5_Data.Mod_Size);
   Count                : Ada.Streams.Stream_Io.Count       := 1;
   Done                 : Boolean                           := False;
 
end Gaudry.Algorithms.Crypto.Md5_2;
